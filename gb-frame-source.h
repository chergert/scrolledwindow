/* gb-frame-source.h
 *
 * Copyright (C) 2010-2016 Christian Hergert <christian@hergert.me>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 */

#ifndef GB_FRAME_SOURCE_H
#define GB_FRAME_SOURCE_H

#include <glib.h>

G_BEGIN_DECLS

guint _gb_frame_source_add (guint       frames_per_sec,
                            GSourceFunc callback,
                            gpointer    user_data);

G_END_DECLS

#endif /* GB_FRAME_SOURCE_H */
